Changes to the upstream tarball provides by the authors of Symfony
==================================================================

Removed prebuilt windows binary
-------------------------------
The binary src/Symfony/Component/Console/Resources/bin/hiddeninput.exe has
been removed for the following reasons:
- Licensing of this file is unclear
- No source code is provided by upstream of Symfony
Additionally it is not needed under Debian at all.


Updating to a new upstream release
==================================
This packaging makes use of uscan from devscripts and git-buildpackage. The
minimum required versions of those are:
- devscripts (>= 2.14.5~)
- git-buildpackage (>= 0.6.13~)
Having those in place you can follow the instructions below to import a new
upstream release and repackage its source.
Note: As of version 0.6.13, git-buildpackage does not import the repackaged
      upstream  source if 'gbp import-orig --uscan' is used. That's why this
      feature of git-buildpackage is not used in the instructions below.

0) Make sure upstream sure upstream's repository is added as a remote
# git remote add upstream git://github.com/symfony/symfony.git

1) Fetch upstream's tags
# git fetch upstream --tags

2) Download and repack upstream's source using uscan
# debian/rules get-orig-source

3) Import the downloaded, repacked and renamed source with git-buildpackage
# gbp import-orig --sign-tags --upstream-vcs-tag=v<VERSION> ../symfony_<VERSION>+dfsg.orig.tar.gz

4) Make sure licensing is correct, especially for images.
   There is script that might be helpful for the image licensing:
# ./debian/licensing/bin/check_image_licenses

5) ...and of course don't forget to update debian/changelog

