<?php

// require_once 'Doctrine/Common/autoload.php'; (already required by Bridge/Doctrine and more)
// require_once 'Fig/Link/autoload.php'; (already required by WebLink)
// require_once 'Twig/autoload.php'; (already required by Bridge/Twig)
// require_once 'Psr/Cache/autoload.php'; (already required by Cache)
// require_once 'Psr/Container/autoload.php'; (already required by DependencyInjection)
// require_once 'Psr/Link/autoload.php'; (already required by WebLink)
// require_once 'Psr/Log/autoload.php'; (already required by Debug and more)
// require_once 'Psr/SimpleCache/autoload.php'; (already required by Cache)

require_once 'Symfony/Component/Asset/autoload.php';// (already *included* by Bridge/Twig)
require_once 'Symfony/Component/BrowserKit/autoload.php'; // (already *included* by HttpKernel)
// require_once 'Symfony/Component/Cache/autoload.php'; (already required by FrameworkBundle and more)
// require_once 'Symfony/Component/ClassLoader/autoload.php'; (already required by FrameworkBundle and more)
// require_once 'Symfony/Component/Config/autoload.php'; (already required by FrameworkBundle)
// require_once 'Symfony/Component/Console/autoload.php'; // (already required by WebServerBundle)
require_once 'Symfony/Component/CssSelector/autoload.php'; // (already *included* by DomCrawler)
// require_once 'Symfony/Component/DependencyInjection/autoload.php'; (already required by FrameworkBundle and more)
// require_once 'Symfony/Component/Debug/autoload.php'; (already required by HttpKernel)
require_once 'Symfony/Bundle/DebugBundle/autoload.php';
require_once 'Symfony/Bridge/Doctrine/autoload.php'; // (already *included* by PropertyInfo)
// require_once 'Symfony/Component/DomCrawler/autoload.php'; (already required by BrowserKit)
require_once 'Symfony/Component/Dotenv/autoload.php';
// require_once 'Symfony/Component/EventDispatcher/autoload.php'; (already required by FrameworkBundle and more)
require_once 'Symfony/Component/ExpressionLanguage/autoload.php'; // (already *included* by Security and more)
// require_once 'Symfony/Component/Filesystem/autoload.php'; (already required by FrameworkBundle and more)
// require_once 'Symfony/Component/Finder/autoload.php'; (already required by FrameworkBundle and more)
require_once 'Symfony/Component/Form/autoload.php'; // (already *included* by Security and more)
require_once 'Symfony/Bundle/FrameworkBundle/autoload.php'; // (already *included* by Form)
// require_once 'Symfony/Component/HttpFoundation/autoload.php'; (already required by FrameworkBundle and more)
// require_once 'Symfony/Component/HttpKernel/autoload.php'; (already required by FrameworkBundle and more)
// require_once 'Symfony/Component/Inflector/autoload.php'; (already required by PropertyAccess and PropertyInfo)
// require_once 'Symfony/Component/Intl/autoload.php'; (already required by Form)
require_once 'Symfony/Component/Ldap/autoload.php'; // (already *included* by Security)
require_once 'Symfony/Component/Lock/autoload.php'; // (already *included* by Console)
require_once 'Symfony/Bridge/Monolog/autoload.php';
// require_once 'Symfony/Component/OptionsResolver/autoload.php'; (already required by Form)
require_once 'Symfony/Component/Process/autoload.php'; // (already *included* by FrameworkBundle and more)
// require_once 'Symfony/Component/PropertyAccess/autoload.php'; (already required by Security and more)
require_once 'Symfony/Component/PropertyInfo/autoload.php'; // (already *included* by FrameworkBundle and more)
require_once 'Symfony/Bridge/ProxyManager/autoload.php'; // (already *included* by DependencyInjection)
// require_once 'Symfony/Component/Routing/autoload.php'; (already required by FrameworkBundle and more)
// require_once 'Symfony/Component/Security/autoload.php'; (already required by SecurityBundle)
// require_once 'Symfony/Component/Security/Core/autoload.php'; (already required by Security and more)
// require_once 'Symfony/Component/Security/Csrf/autoload.php'; (already required by Security and more)
// require_once 'Symfony/Component/Security/Guard/autoload.php'; (already required by Security and more)
// require_once 'Symfony/Component/Security/Http/autoload.php'; (already required by Security and more)
require_once 'Symfony/Bundle/SecurityBundle/autoload.php';
require_once 'Symfony/Component/Serializer/autoload.php'; // (already *included* by FrameworkBundle and more)
// require_once 'Symfony/Component/Stopwatch/autoload.php'; (already required by EventDispatcher and more)
require_once 'Symfony/Component/Templating/autoload.php'; // (already *included* by TwigBridge)
// require_once 'Symfony/Component/Translation/autoload.php'; (already required by Validator)
// require_once 'Symfony/Bridge/Twig/autoload.php'; (already required by TwigBundle and more)
require_once 'Symfony/Bundle/TwigBundle/autoload.php';
require_once 'Symfony/Component/Validator/autoload.php'; // (already *included* by FrameworkBundle and more)
// require_once 'Symfony/Component/VarDumper/autoload.php'; (already required by DebugBundle)
require_once 'Symfony/Component/WebLink/autoload.php'; // (already *included* by FrameworkBundle and TwigBridge)
require_once 'Symfony/Bundle/WebProfilerBundle/autoload.php';
require_once 'Symfony/Bundle/WebServerBundle/autoload.php';
require_once 'Symfony/Component/Workflow/autoload.php';
require_once 'Symfony/Component/Yaml/autoload.php'; // (already *included* by FrameworkBundle and more)

// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
// this is an autogenerated file - do not edit
spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = array(
                'symfony\\bundle\\fullstack' => '/Bundle/FullStack.php'
            );
        }
        $cn = strtolower($class);
        if (isset($classes[$cn])) {
            require __DIR__ . $classes[$cn];
        }
    },
    true,
    false
);
// @codeCoverageIgnoreEnd
