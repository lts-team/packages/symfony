<?php
// require:
require_once 'Twig/autoload.php';

// suggest:
if (stream_resolve_include_path('Symfony/Component/Finder/autoload.php')){
    include_once 'Symfony/Component/Finder/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Asset/autoload.php')){
    include_once 'Symfony/Component/Asset/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Form/autoload.php')){
    include_once 'Symfony/Component/Form/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/HttpKernel/autoload.php')){
    include_once 'Symfony/Component/HttpKernel/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Routing/autoload.php')){
    include_once 'Symfony/Component/Routing/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Templating/autoload.php')){
    include_once 'Symfony/Component/Templating/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Translation/autoload.php')){
    include_once 'Symfony/Component/Translation/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Yaml/autoload.php')){
    include_once 'Symfony/Component/Yaml/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Security/autoload.php')){
    include_once 'Symfony/Component/Security/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/Stopwatch/autoload.php')){
    include_once 'Symfony/Component/Stopwatch/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/VarDumper/autoload.php')){
    include_once 'Symfony/Component/VarDumper/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/ExpressionLanguage/autoload.php')){
    include_once 'Symfony/Component/ExpressionLanguage/autoload.php';
}
if (stream_resolve_include_path('Symfony/Component/WebLink/autoload.php')){
    include_once 'Symfony/Component/WebLink/autoload.php';
}

// @codingStandardsIgnoreFile
// @codeCoverageIgnoreStart
// this is an autogenerated file - do not edit
spl_autoload_register(
    function($class) {
        static $classes = null;
        if ($classes === null) {
            $classes = array(
                ___CLASSLIST___
            );
        }
        $cn = strtolower($class);
        if (isset($classes[$cn]) and file_exists(___BASEDIR___$classes[$cn])) {
            require ___BASEDIR___$classes[$cn];
        }
    }
);
// @codeCoverageIgnoreEnd
